import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';

describe('UserController', () => {
  let controller: UserController;

  const mockUserService = {
    create: jest.fn(),
    findAll: jest.fn(),
    findOne: jest.fn(),
    update: jest.fn(),
    remove: jest.fn()
  };
  const mockUserCreateData = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@email.com'
  };

  const mockUserFindAllData = [
    {
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@email.com'
    },
    {
      firstName: 'Jane',
      lastName: 'Doe',
      email: 'janedoe@email.com'
    }
  ];

  const mockUserFindOneData = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@email.com',
    id: 1
  };

  const mockUserUpdateData = {
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@email.com'
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        {
          provide: UserService,
          useValue: mockUserService
        }
      ]
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('CREATE: should return a user object', async () => {
    const createdUser = { ...mockUserCreateData, id: 1 };
    mockUserService.create.mockResolvedValueOnce(createdUser);
    const result = await controller.create(mockUserCreateData);
    expect(result).toBe(createdUser);
    expect(mockUserService.create).toBeCalledTimes(1);
    expect(mockUserService.create).toBeCalledWith(mockUserCreateData);
  });

  it('FINDALL: should return an array of users', async () => {
    mockUserService.findAll.mockResolvedValueOnce(mockUserFindAllData);
    const result = await controller.findAll();
    expect(result).toBe(mockUserFindAllData);
    expect(mockUserService.findAll).toBeCalledTimes(1);
    expect(mockUserService.findAll).toBeCalledWith();
  });

  it('FINDONE: should return a user', async () => {
    mockUserService.findOne.mockResolvedValueOnce(mockUserFindOneData);
    const result = await controller.findOne(1);
    expect(result).toBe(mockUserFindOneData);
    expect(mockUserService.findOne).toBeCalledTimes(1);
    expect(mockUserService.findOne).toBeCalledWith(1);
  });

  it('UPDATE: should return a user', async () => {
    const updatedUser = { ...mockUserUpdateData, id: 1 };
    mockUserService.update.mockResolvedValueOnce(updatedUser);
    const result = await controller.update(1, mockUserUpdateData);
    expect(result).toBe(updatedUser);
    expect(mockUserService.update).toBeCalledTimes(1);
    expect(mockUserService.update).toBeCalledWith(1, mockUserUpdateData);
  });

  it('REMOVE: should not return anything', async () => {
    mockUserService.remove.mockResolvedValueOnce(true);
    const result = await controller.remove(1);
    expect(result).toBe(true);
    expect(mockUserService.remove).toBeCalledTimes(1);
    expect(mockUserService.remove).toBeCalledWith(1);
  });
});
