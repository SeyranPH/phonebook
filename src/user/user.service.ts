import { HttpException, ConflictException, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { PhoneNumber } from '../phone-number/phone-number.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(PhoneNumber)
    private phoneNumberRepository: Repository<User>
  ) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = new User();
    user.firstName = createUserDto.firstName;
    user.lastName = createUserDto.lastName;
    if (createUserDto.email) {
      const emailExists = await this.userRepository.findOne({
        email: createUserDto.email
      });
      if (emailExists) {
        throw new ConflictException('Email already exists');
      }
      user.email = createUserDto.email;
    }
    return await this.userRepository.save(user);
  }

  async findAll(): Promise<User[]> {
    const result = await this.userRepository.find({
      relations: ['phoneNumber']
    });
    if (!result) {
      throw new HttpException('User not found', 404);
    }
    return result;
  }

  async findOne(id: number): Promise<User> {
    const result = await this.userRepository.findOne(id, {
      relations: ['phoneNumber']
    });
    if (!result) {
      throw new HttpException('User not found', 404);
    }
    return result;
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const currentUser = await this.userRepository.findOne(id);
    if (!currentUser) {
      throw new HttpException('User not found', 404);
    }

    if (updateUserDto.email) {
      const emailExists = await this.userRepository.findOne({
        email: updateUserDto.email
      });
      if (emailExists) {
        throw new ConflictException('Email already exists');
      }
    }

    if (updateUserDto.phoneNumber) {
      const currentPhoneNumber = await this.phoneNumberRepository.findOne({
        where: { id: updateUserDto.phoneNumber }
      });
      if (!currentPhoneNumber) {
        throw new HttpException('Phone number not found', 406);
      }
      const newPhoneNumber = Object.assign(currentPhoneNumber, { user: id });
      await this.phoneNumberRepository.save(newPhoneNumber);
      delete updateUserDto.phoneNumber;
    }

    Object.assign(currentUser, updateUserDto);
    const result = await this.userRepository.save(currentUser);
    return result;
  }

  async remove(id: number) {
    const currentUser = await this.userRepository.findOne(id);
    if (!currentUser) {
      throw new HttpException('user not found', 404);
    }
    await this.userRepository.delete(id);
    return true;
  }
}
