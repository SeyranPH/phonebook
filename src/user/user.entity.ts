import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { PhoneNumber } from '../phone-number/phone-number.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  firstName: string;

  @Column({ nullable: false })
  lastName: string;

  @Column({ unique: true, nullable: true })
  email?: string;

  @OneToMany((type) => PhoneNumber, (phoneNumber) => phoneNumber.user, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  phoneNumber?: PhoneNumber[];
}
