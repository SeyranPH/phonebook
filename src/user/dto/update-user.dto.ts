import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional, Length } from 'class-validator';

export class UpdateUserDto {
  @ApiProperty()
  @IsOptional()
  @Length(2, 20)
  firstName?: string;

  @ApiProperty()
  @IsOptional()
  @Length(2, 30)
  lastName?: string;

  @ApiProperty()
  @IsOptional()
  @IsEmail()
  email?: string;

  @ApiProperty()
  @IsOptional()
  @Length(3, 20)
  phoneNumber?: string;
}
