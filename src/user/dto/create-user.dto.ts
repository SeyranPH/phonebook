import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional, Length } from 'class-validator';

export class CreateUserDto {
  @ApiProperty()
  @Length(2, 20)
  firstName: string;

  @ApiProperty()
  @Length(2, 30)
  lastName: string;

  @ApiProperty()
  @IsOptional()
  @IsEmail()
  email?: string;
}
