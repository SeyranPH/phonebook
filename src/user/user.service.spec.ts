import { HttpException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PhoneNumber } from '../phone-number/phone-number.entity';
import { User } from './user.entity';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;

  const mockUserRepository = {
    save: jest.fn(),
    find: jest.fn(),
    findOne: jest.fn(),
    delete: jest.fn()
  };

  const userRepositorySave = {
    id: 1,
    firstName: 'John',
    lastName: 'Doe'
  };

  const userRepositoryFind = [
    {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@email.com'
    }
  ];

  const userRepositoryFindOne = userRepositorySave;

  const mockPhoneNumberRepository = {
    save: jest.fn(),
    findOne: jest.fn()
  };

  const phoneNumberRepositoryFindOne = {
    id: 1,
    country: 'Armenia',
    operator: 'Beeline',
    number: '0991909909',
    user: {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@email.com'
    }
  };

  const phoneNumberRepositorySave = {
    id: 1,
    country: 'Armenia',
    operator: 'Beeline',
    number: '0991909909'
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useValue: mockUserRepository
        },
        {
          provide: getRepositoryToken(PhoneNumber),
          useValue: mockPhoneNumberRepository
        }
      ]
    }).compile();

    mockUserRepository.save.mockResolvedValue(userRepositorySave);
    mockUserRepository.find.mockResolvedValue(userRepositoryFind);
    mockUserRepository.findOne.mockResolvedValue(userRepositoryFindOne);
    mockUserRepository.delete.mockResolvedValue(true);

    mockPhoneNumberRepository.save.mockResolvedValue(phoneNumberRepositorySave);
    mockPhoneNumberRepository.findOne.mockResolvedValue(
      phoneNumberRepositoryFindOne
    );

    service = module.get<UserService>(UserService);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('CREATE (with email): should return user', async () => {
    mockUserRepository.findOne.mockResolvedValueOnce(undefined);
    mockUserRepository.save.mockResolvedValueOnce({
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@email.com'
    });
    const result = await service.create({
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@email.com'
    });
    expect(result).toStrictEqual({
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@email.com'
    });
  });

  it('CREATE (without email): should return user without email', async () => {
    const result = await service.create({
      firstName: 'John',
      lastName: 'Doe'
    });

    expect(result).toStrictEqual(userRepositorySave);
  });

  it('FINDALL (with existing data): should return user array with phonenumbers', async () => {
    const result = await service.findAll();
    expect(result).toStrictEqual(userRepositoryFind);
  });

  it('FINDALL (without existing data): should throw 404 exception', async () => {
    mockUserRepository.find.mockResolvedValueOnce(undefined);
    try {
      await service.findAll();
    } catch (error) {
      expect(error).toStrictEqual(new HttpException('User not found', 404));
    }
  });

  it('FINDONE (with existing user): should return a user with phonenumber', async () => {
    const result = await service.findOne(1);
    expect(result).toStrictEqual(userRepositoryFindOne);
  });

  it('FINDONE (without existing user): should throw 404 exception', async () => {
    mockUserRepository.findOne.mockResolvedValueOnce(undefined);
    try {
      await service.findOne(1);
    } catch (error) {
      expect(error).toStrictEqual(new HttpException('User not found', 404));
    }
  });

  it('UPDATE (with unexisting user): should throw 404 exception', async () => {
    mockUserRepository.findOne.mockResolvedValueOnce(undefined);
    try {
      await service.update(1, { firstName: 'Jane' });
    } catch (err) {
      expect(err).toStrictEqual(new HttpException('User not found', 404));
    }
  });

  it('UPDATE (with existing user and without phonenumber): should return updated user', async () => {
    const result = await service.update(1, { firstName: 'John' });
    expect(result).toStrictEqual(userRepositoryFindOne);
  });

  it('UPDATE (with existing user and with unexisting phonenumber): should throw 406 exception', async () => {
    mockPhoneNumberRepository.findOne.mockResolvedValueOnce(undefined);
    try {
      await service.update(1, {
        firstName: 'Jane',
        phoneNumber: '99999999999'
      });
    } catch (err) {
      expect(err).toStrictEqual(
        new HttpException('Phone number not found', 406)
      );
    }
  });

  it('UPDATE (with existing user and with existing phonenumber): should return updated user', async () => {
    const result = await service.update(1, { firstName: 'John' });
    expect(result).toStrictEqual(userRepositoryFindOne);
  });

  it('REMOVE: should return true', async () => {
    const result = await service.remove(1);
    expect(result).toBe(true);
  });
});
