import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotAcceptableResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger';

@ApiTags('user')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  @ApiOperation({ summary: 'creates a new user' })
  @ApiCreatedResponse({
    description: 'Data has been successfully created.'
  })
  @ApiBadRequestResponse({
    description: 'The request failed validation.'
  })
  @ApiConflictResponse({
    description: 'The email of new user already exists.'
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal error happened'
  })
  async create(@Body() createUserDto: CreateUserDto) {
    try {
      const newUser = this.userService.create(createUserDto);

      return newUser;
    } catch (error) {
      return;
    }
  }

  @Get()
  @ApiOperation({ summary: 'searches for all users' })
  @ApiCreatedResponse({
    description: 'Data has been found.'
  })
  @ApiNotFoundResponse({
    description: 'Data has not been found'
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal error happened'
  })
  findAll() {
    return this.userService.findAll();
  }

  @Get(':id')
  @ApiOperation({ summary: 'searches for a specific user' })
  @ApiCreatedResponse({
    description: 'Data has been found.'
  })
  @ApiNotFoundResponse({
    description: 'Data with specified id has not been found'
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal error happened'
  })
  findOne(@Param('id') id: number) {
    return this.userService.findOne(id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'updates a user' })
  @ApiCreatedResponse({
    description: 'User has been successfully updated.'
  })
  @ApiNotFoundResponse({
    description: 'User has not been found'
  })
  @ApiBadRequestResponse({
    description: 'The request failed validation.'
  })
  @ApiConflictResponse({
    description: 'The email of new user already exists.'
  })
  @ApiNotAcceptableResponse({
    description: "The phone number doesn't exist."
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal error happened'
  })
  update(@Param('id') id: number, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(id, updateUserDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'removes a user' })
  @ApiCreatedResponse({
    description: 'User has been successfully deleted.'
  })
  @ApiNotFoundResponse({
    description: 'User has not been found'
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal error happened'
  })
  remove(@Param('id') id: number) {
    return this.userService.remove(id);
  }
}
