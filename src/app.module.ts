import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';

import { UserModule } from './user/user.module';
import { User } from './user/user.entity';
import { envVars } from './config';

import { PhoneNumberModule } from './phone-number/phone-number.module';
import { PhoneNumber } from './phone-number/phone-number.entity';
import { config } from 'dotenv';
@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: envVars.postgres.host,
      username: envVars.postgres.username,
      password: envVars.postgres.password,
      port: envVars.postgres.port,
      database: envVars.postgres.database,
      entities: [User, PhoneNumber],
      synchronize: true
    }),
    PhoneNumberModule,
    UserModule
  ]
})
export class AppModule {}
