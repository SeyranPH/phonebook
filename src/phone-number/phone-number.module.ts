import { Module } from '@nestjs/common';
import { PhoneNumberService } from './phone-number.service';
import { PhoneNumberController } from './phone-number.controller';
import { PhoneNumber } from './phone-number.entity';
import { User } from '../user/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    TypeOrmModule.forFeature([PhoneNumber]),
    TypeOrmModule.forFeature([User])
  ],
  controllers: [PhoneNumberController],
  providers: [PhoneNumberService]
})
export class PhoneNumberModule {}
