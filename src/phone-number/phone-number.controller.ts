import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException
} from '@nestjs/common';
import { PhoneNumberService } from './phone-number.service';
import { CreatePhoneNumberDto } from './dto/create-phone-number.dto';
import { UpdatePhoneNumberDto } from './dto/update-phone-number.dto';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags
} from '@nestjs/swagger';

@ApiTags('phone-number')
@Controller('phone-number')
export class PhoneNumberController {
  constructor(private readonly phoneNumberService: PhoneNumberService) {}

  @Post()
  @ApiOperation({ summary: 'creates a phone number' })
  @ApiCreatedResponse({
    description: 'The record has been successfully created.'
  })
  @ApiBadRequestResponse({
    description: 'The request failed validation.'
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal error happened'
  })
  async create(@Body() createPhoneNumberDto: CreatePhoneNumberDto) {
    const phoneNumber = await this.phoneNumberService.create(
      createPhoneNumberDto
    );
    if (phoneNumber) {
      return phoneNumber;
    } else throw new HttpException('Unable to create phone number', 500);
  }

  @Get()
  @ApiOperation({ summary: 'searches for all phone numbers' })
  @ApiOkResponse({
    description: 'Data has been successfully found.'
  })
  @ApiNotFoundResponse({
    description: 'Data was not found.'
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal error happened'
  })
  async findAll() {
    return await this.phoneNumberService.findAll();
  }

  @Get(':id')
  @ApiOperation({ summary: 'searches for a specified phone number' })
  @ApiOkResponse({
    description: 'Data has been successfully found.'
  })
  @ApiNotFoundResponse({
    description: 'Data with specified id was not found.'
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal error happened'
  })
  findOne(@Param('id') id: number) {
    return this.phoneNumberService.findOne(+id);
  }

  @Patch(':id')
  @ApiOperation({ summary: 'updates a phone number' })
  @ApiOkResponse({
    description: 'Data has been successfully updated.'
  })
  @ApiNotFoundResponse({
    description: 'New phone number owner was not found'
  })
  @ApiBadRequestResponse({
    description: 'The request failed validation.'
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal error happened'
  })
  update(
    @Param('id') id: number,
    @Body() updatePhoneNumberDto: UpdatePhoneNumberDto
  ) {
    return this.phoneNumberService.update(id, updatePhoneNumberDto);
  }

  @Delete(':id')
  @ApiOperation({ summary: 'removes a phone number' })
  @ApiOkResponse({
    description: 'Data has been successfully deleted.'
  })
  @ApiNotFoundResponse({
    description: 'Phone number was not found'
  })
  @ApiInternalServerErrorResponse({
    description: 'Internal error happened'
  })
  remove(@Param('id') id: number) {
    return this.phoneNumberService.remove(id);
  }
}
