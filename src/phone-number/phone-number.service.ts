import {
  ConflictException,
  HttpException,
  Injectable,
  NotFoundException
} from '@nestjs/common';
import { CreatePhoneNumberDto } from './dto/create-phone-number.dto';
import { UpdatePhoneNumberDto } from './dto/update-phone-number.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PhoneNumber } from './phone-number.entity';
import { User } from '../user/user.entity';

@Injectable()
export class PhoneNumberService {
  constructor(
    @InjectRepository(PhoneNumber)
    private phoneNumberRepository: Repository<PhoneNumber>,
    @InjectRepository(User)
    private userRepository: Repository<User>
  ) {}

  async create(createPhoneNumberDto: CreatePhoneNumberDto) {
    const phoneNumber = new PhoneNumber();
    const numberExists = await this.phoneNumberRepository.findOne({
      number: createPhoneNumberDto.number
    });
    if (numberExists) {
      throw new ConflictException('Number already exists');
    }
    phoneNumber.number = createPhoneNumberDto.number;
    const currentOwner = await this.userRepository.findOne(
      createPhoneNumberDto.user
    );
    if (!currentOwner) {
      throw new HttpException('User not found', 404);
    }
    phoneNumber.user = createPhoneNumberDto.user;

    if (createPhoneNumberDto.operator) {
      phoneNumber.operator = createPhoneNumberDto.operator;
    }

    if (createPhoneNumberDto.country) {
      phoneNumber.country = createPhoneNumberDto.country;
    }
    const newPhoneNumber = await this.phoneNumberRepository.save(phoneNumber);
    return newPhoneNumber;
  }

  async findAll() {
    const result = await this.phoneNumberRepository.find({
      relations: ['user']
    });
    if (!result) {
      throw new HttpException('Phone number not found', 404);
    }
    return result;
  }

  async findOne(id: number) {
    const result = await this.phoneNumberRepository.findOne(id, {
      relations: ['user']
    });
    if (!result) {
      throw new HttpException('Phone number not found', 404);
    }
    return result;
  }

  async update(id: number, updatePhoneNumberDto: UpdatePhoneNumberDto) {
    const currentPhoneNumber = await this.phoneNumberRepository.findOne(id);
    if (!currentPhoneNumber) {
      throw new NotFoundException('phone number not found');
    }

    if (updatePhoneNumberDto.user) {
      const newOwner = await this.userRepository.findOne(
        updatePhoneNumberDto.user
      );
      if (!newOwner) {
        throw new HttpException('New number owner is not found', 404);
      }
    }

    Object.assign(currentPhoneNumber, updatePhoneNumberDto);
    const updatedPhoneNumber = await this.phoneNumberRepository.save(
      currentPhoneNumber
    );
    return updatedPhoneNumber;
  }

  async remove(id: number) {
    const currentPhoneNumber = await this.phoneNumberRepository.findOne(id);
    if (!currentPhoneNumber) {
      throw new HttpException('Phone number not found', 404);
    }
    await this.phoneNumberRepository.delete(id);
    return true;
  }
}
