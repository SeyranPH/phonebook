import { Test, TestingModule } from '@nestjs/testing';
import { PhoneNumberController } from './phone-number.controller';
import { PhoneNumberService } from './phone-number.service';

describe('PhoneNumberController', () => {
  let controller: PhoneNumberController;

  const mockPhoneNumberService = {
    create: jest.fn(),
    findAll: jest.fn(),
    findOne: jest.fn(),
    update: jest.fn(),
    remove: jest.fn()
  };

  const phoneNumberExample = {
    id: 1,
    country: 'Armenia',
    operator: 'Beeline',
    number: '099190990',
    user: {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@email.com'
    }
  };

  const mockPhoneNumberCreateData = {
    country: 'Armenia',
    operator: 'Beeline',
    number: '099190990',
    user: 1
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PhoneNumberController],
      providers: [
        {
          provide: PhoneNumberService,
          useValue: mockPhoneNumberService
        }
      ]
    }).compile();
    controller = module.get<PhoneNumberController>(PhoneNumberController);
  });

  it('CREATE: should return phone number object', async () => {
    const createdPhoneNumber = { ...mockPhoneNumberCreateData, id: 1 };
    mockPhoneNumberService.create.mockResolvedValueOnce(createdPhoneNumber);
    const result = await controller.create(mockPhoneNumberCreateData);
    expect(result).toBe(createdPhoneNumber);
    expect(mockPhoneNumberService.create).toBeCalledTimes(1);
    expect(mockPhoneNumberService.create).toBeCalledWith(
      mockPhoneNumberCreateData
    );
  });

  it('FINDALL: should return array of phone number objects', async () => {
    mockPhoneNumberService.findAll.mockResolvedValueOnce([phoneNumberExample]);
    const result = await controller.findAll();
    expect(result).toStrictEqual([phoneNumberExample]);
    expect(mockPhoneNumberService.findAll).toBeCalledTimes(1);
    expect(mockPhoneNumberService.findAll).toBeCalledWith();
  });

  it('FINDONE: should return a phone number object', async () => {
    mockPhoneNumberService.findOne.mockResolvedValueOnce(phoneNumberExample);
    const result = await controller.findOne(1);
    expect(result).toBe(phoneNumberExample);
    expect(mockPhoneNumberService.findOne).toBeCalledTimes(1);
    expect(mockPhoneNumberService.findOne).toBeCalledWith(1);
  });

  it('UPDATE: should return the updated phone number object', async () => {
    mockPhoneNumberService.update.mockResolvedValueOnce(phoneNumberExample);
    const result = await controller.update(1, { operator: 'Beeline' });
    expect(result).toBe(phoneNumberExample);
    expect(mockPhoneNumberService.update).toBeCalledTimes(1);
    expect(mockPhoneNumberService.update).toBeCalledWith(1, {
      operator: 'Beeline'
    });
  });

  it('REMOVE: should return true', async () => {
    mockPhoneNumberService.remove.mockResolvedValueOnce(true);
    const result = await controller.remove(1);
    expect(result).toBe(true);
    expect(mockPhoneNumberService.remove).toBeCalledTimes(1);
    expect(mockPhoneNumberService.remove).toBeCalledWith(1);
  });
});
