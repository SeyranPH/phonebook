import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, Length } from 'class-validator';

export class CreatePhoneNumberDto {
  @ApiProperty()
  @IsOptional()
  country?: string;

  @ApiProperty()
  @IsOptional()
  operator?: string;

  @ApiProperty()
  @Length(4, 20)
  number: string;

  @ApiProperty()
  @IsNumber()
  user: number;
}
