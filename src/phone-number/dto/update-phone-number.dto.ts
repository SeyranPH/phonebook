import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, Length } from 'class-validator';

export class UpdatePhoneNumberDto {
  @ApiProperty()
  @IsOptional()
  country?: string;

  @ApiProperty()
  @IsOptional()
  operator?: string;

  @ApiProperty()
  @Length(4, 20)
  @IsOptional()
  number?: string;

  @ApiProperty()
  @IsNumber()
  @IsOptional()
  user?: number;
}
