import { HttpException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { PhoneNumber } from './phone-number.entity';
import { User } from '../user/user.entity';
import { PhoneNumberService } from './phone-number.service';

describe('PhoneNumberService', () => {
  let service: PhoneNumberService;

  const mockUserRepository = {
    findOne: jest.fn()
  };

  const userRepositoryFindOne = {
    id: 1,
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@email.com'
  };

  const mockPhoneNumberRepository = {
    save: jest.fn(),
    find: jest.fn(),
    findOne: jest.fn(),
    delete: jest.fn()
  };

  const phoneNumberRepositorySave = {
    id: 1,
    country: 'Armenia',
    operator: 'Beeline',
    number: '0991909909',
    user: 1
  };

  const phoneNumberRepositoryFindOne = {
    id: 1,
    country: 'Armenia',
    operator: 'Beeline',
    number: '0991909909',
    user: {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'johndoe@email.com'
    }
  };

  const phoneNumberRepositoryFind = [phoneNumberRepositoryFindOne];

  beforeEach(async () => {
    mockUserRepository.findOne.mockResolvedValue(userRepositoryFindOne);

    mockPhoneNumberRepository.save.mockResolvedValue(phoneNumberRepositorySave);
    mockPhoneNumberRepository.findOne.mockResolvedValue(
      phoneNumberRepositoryFindOne
    );
    mockPhoneNumberRepository.find.mockResolvedValue(phoneNumberRepositoryFind);
    mockPhoneNumberRepository.delete.mockResolvedValue(true);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PhoneNumberService,
        {
          provide: getRepositoryToken(User),
          useValue: mockUserRepository
        },
        {
          provide: getRepositoryToken(PhoneNumber),
          useValue: mockPhoneNumberRepository
        }
      ]
    }).compile();

    service = module.get<PhoneNumberService>(PhoneNumberService);
  });

  it('CREATE (with unexisting user): should throw 404 exception', async () => {
    mockUserRepository.findOne.mockResolvedValueOnce(undefined);
    mockPhoneNumberRepository.findOne.mockResolvedValueOnce(undefined);

    try {
      await service.create({
        country: 'Armenia',
        operator: 'Beeline',
        number: '0991909909',
        user: 1
      });
    } catch (error) {
      expect(error).toStrictEqual(new HttpException('User not found', 404));
    }
  });

  it('CREATE (with existing user): should return phone number', async () => {
    mockPhoneNumberRepository.findOne.mockResolvedValueOnce(undefined);

    const result = await service.create({
      country: 'Armenia',
      operator: 'Beeline',
      number: '0991909909',
      user: 1
    });
    expect(result).toStrictEqual(phoneNumberRepositorySave);
  });

  it('CREATE (with existing user, without optional fields): should return phone number without optional fields', async () => {
    mockPhoneNumberRepository.save.mockResolvedValueOnce({
      id: 1,
      number: '0991909909',
      user: 1
    });
    mockPhoneNumberRepository.findOne.mockResolvedValueOnce(undefined);

    const result = await service.create({
      number: '0991909909',
      user: 1
    });
    expect(result).toStrictEqual({
      id: 1,
      number: '0991909909',
      user: 1
    });
  });

  it('FINDALL (with unexisting phone number): should throw 404 exception', async () => {
    mockPhoneNumberRepository.find.mockResolvedValueOnce(undefined);

    try {
      await service.findAll();
    } catch (error) {
      expect(error).toStrictEqual(
        new HttpException('Phone number not found', 404)
      );
    }
  });

  it('FINDALL (with existing phone number): should return phone number array', async () => {
    const result = await service.findAll();
    expect(result).toStrictEqual(phoneNumberRepositoryFind);
  });

  it('FINDONE (with unexisting phone number): should throw 404 exception', async () => {
    mockPhoneNumberRepository.findOne.mockResolvedValueOnce(undefined);

    try {
      await service.findOne(1);
    } catch (error) {
      expect(error).toStrictEqual(
        new HttpException('Phone number not found', 404)
      );
    }
  });

  it('FINDONE (with existing phone number): should return phone number with user', async () => {
    const result = await service.findOne(1);
    expect(result).toStrictEqual(phoneNumberRepositoryFindOne);
  });

  it('UPDATE (without user): should return updated phone number', async () => {
    const result = await service.update(1, { operator: 'Beeline' });
    expect(result).toStrictEqual(phoneNumberRepositorySave);
  });

  it('UPDATE (with valid user): should return updated phone number', async () => {
    const result = await service.update(1, { operator: 'Beeline', user: 1 });
    expect(result).toBe(phoneNumberRepositorySave);
  });

  it('UPDATE (with invalid user): should throw 404 exception', async () => {
    mockUserRepository.findOne.mockResolvedValueOnce(undefined);
    try {
      await service.update(1, { operator: 'Beeline', user: 1 });
    } catch (error) {
      expect(error).toStrictEqual(
        new HttpException('New number owner is not found', 404)
      );
    }
  });

  it('REMOVE: should return true', async () => {
    const result = await service.remove(1);
    expect(result).toBe(true);
  });
});
