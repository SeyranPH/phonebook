# Phonebook app

This is a phonebook app. It might be used for adding users and their phone numbers

## Prerequisites

docker
docker-compose
node v14.16.0

## Getting started

For local setup you need to download this repo

```shell
git clone https://gitlab.com/SeyranPH/phonebook
```

Then from the app directory run:

```shell
docker-compose build
docker-compose up
```

Notice .env.sample file. You need to create your own .env file with appropriate data

## Features

For API documentation visit /api
